import random
from threading import Thread,Semaphore
from time import sleep
from timeit import Timer
import argparse
from Lightswitch import Lightswitch

#shared variables
multiplex=Semaphore(5)
turnstile=Semaphore(1)
rope=Semaphore(1)
e_switch=Lightswitch()
w_switch=Lightswitch()

#for majority rules
west=0#number of baboons from west
east=0#number of baboons from east
mutex=Semaphore(1)
westTurn=Semaphore(1)
eastTurn=Semaphore(1)
westLine=Semaphore(0)#line for waiting baboons from west
eastLine=Semaphore(0)#line for waiting baboons from east


#code for baboon crossing from west
def wCross(rng):
#     turnstile.acquire()
    w_switch.lock(rope)
#     turnstile.release()
    
    multiplex.acquire()
    #cross rope
    sleep(rng.random()+0.5)
    multiplex.release()
    
    w_switch.unlock(rope)
    
#code for baboon crossing from east
def eCross(rng):
#     turnstile.acquire()
    e_switch.lock(rope)
#     turnstile.release()
    
    multiplex.acquire()
    #cross rope
    sleep(rng.random()+0.5)
    multiplex.release()
    
    e_switch.unlock(rope)
    
def goFromWest(rng):
    global west
    global east
 
    mutex.acquire()
    west+=1
    if west==5:
        for _ in range(5):
            westLine.release()
    if west+east<10:
        for _ in range(west):
            westLine.release()
            
    mutex.release()
    westLine.acquire()
        
    wCross(rng)
    
    mutex.acquire()
    west-=1        
    mutex.release()
    sleep(rng.random()+0.5)#wait to cross back 
    
def goFromEast(rng):
    global east
    global west
    
    mutex.acquire()
    east+=1
    if east==5:
        for _ in range(5):
            eastLine.release()
    if west+east<10:
        for _ in range(east):
            eastLine.release()
    mutex.release()
    eastLine.acquire()
    
    eCross(rng)
    
    mutex.acquire()
    east-=1        
    mutex.release()
    sleep(rng.random()+0.5)#wait to cross back
    
def baboon(num,cross):
    rng=random.Random()
    rng.seed(num)
    if rng.random()<0.5:#start crossing from west
        for _ in range((int)(cross/2)):  
#             print("baboon {} goes from west".format(num))  
            goFromWest(rng)
#             print("baboon {} goes from east".format(num))
            goFromEast(rng)
    else:#start crossing from east
        for _ in range((int)(cross/2)):
#             print("baboon {} goes from east".format(num))
            goFromEast(rng)
#             print("baboon {} goes from west".format(num))  
            goFromWest(rng)
    
def totime(num,cross):
    bab="baboon("+str(num)+","+str(cross)+")"
    timer=Timer(bab,"from baboon3 import baboon")
    tim=timer.timeit(1)
    print("Baboon {} finished in {:0.4f}s".format(num,tim))

def total(baboon,cross):
    print("Timing 3 simulations with {} baboons, {} crossings:".format(baboon,cross))

    for _ in range(3):
        print('-'*30)
        ts=[Thread(target=totime,args=[j,cross]) for j in range(baboon)]
        for t in ts:t.start()
        for t in ts:t.join()
        
    
if __name__=='__main__':
    parser = argparse.ArgumentParser(description='Baboon Crossing Rope3')
    parser.add_argument('--baboon','-b',
                        type=int,
                        default=10,
                        help='number of baboons',
                        metavar='numBaboon')
    parser.add_argument('--cross','-c',
                        type=int,
                        default=6,
                        help='number of crossings per baboon',
                        metavar='numCross')

    args = parser.parse_args()
    tot="total("+str(args.baboon)+","+str(args.cross)+")"
    timer=Timer(tot,"from baboon3 import total")
    tim=timer.timeit(1)
    print("-"*30)
    print("Total time elapsed: {}".format(tim))
    print("Avg. time per run: {}".format(tim/3))
    
    
        
        
        
