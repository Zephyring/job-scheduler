import random
from threading import Thread,Semaphore
from time import sleep
from timeit import Timer
import argparse
from Lightswitch import Lightswitch

#shared variables
multiplex=Semaphore(5)
turnstile=Semaphore(1)
rope=Semaphore(1)
e_switch=Lightswitch()
w_switch=Lightswitch()


#code for baboon crossing from west
def wCross(rng):
    turnstile.acquire()
    w_switch.lock(rope)
    turnstile.release()
    
    multiplex.acquire()
    #cross rope
    sleep(rng.random()+0.5)
    multiplex.release()
    
    w_switch.unlock(rope)
    
#code for baboon crossing from east
def eCross(rng):
    turnstile.acquire()
    e_switch.lock(rope)
    turnstile.release()
    
    multiplex.acquire()
    #cross rope
    sleep(rng.random()+0.5)
    multiplex.release()
    
    e_switch.unlock(rope)

def baboon(num,cross):
    rng=random.Random()
    rng.seed(num)
    if rng.random()<0.5:#start crossing from west
        for _ in range((int)(cross/2)):    
            wCross(rng)
            sleep(rng.random()+0.5)#wait to cross back 
            eCross(rng)
            sleep(rng.random()+0.5)#wait to cross back
    else:#start crossing from east
        for _ in range((int)(cross/2)):
            eCross(rng)
            sleep(rng.random()+0.5)#wait to cross back
            wCross(rng)
            sleep(rng.random()+0.5)#wait to cross back
    
def totime(num,cross):
    bab="baboon("+str(num)+","+str(cross)+")"
    timer=Timer(bab,"from baboon1 import baboon")
    tim=timer.timeit(1)
    print("Baboon {} finished in {:0.4f}s".format(num,tim))

def total(baboon,cross):
    print("Timing 3 simulations with {} baboons, {} crossings:".format(baboon,cross))
    
    for _ in range(3):
        print('-'*30)
        ts=[Thread(target=totime,args=[j,cross]) for j in range(baboon)]
        for t in ts:t.start()
        for t in ts:t.join()
        
    
if __name__=='__main__':
    parser = argparse.ArgumentParser(description='Baboon Crossing Rope1')
    parser.add_argument('--baboon','-b',
                        type=int,
                        default=10,
                        help='number of baboons',
                        metavar='numBaboon')
    parser.add_argument('--cross','-c',
                        type=int,
                        default=6,
                        help='number of crossings per baboon',
                        metavar='numCross')
                        
    args = parser.parse_args()
    tot="total("+str(args.baboon)+","+str(args.cross)+")"
    timer=Timer(tot,"from baboon1 import total")
    tim=timer.timeit(1)
    print("-"*30)
    print("Total time elapsed: {}".format(tim))
    print("Avg. time per run: {}".format(tim/3))
    
    
        
        
        
