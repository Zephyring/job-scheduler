import random
from threading import Thread,Semaphore
from time import sleep
from timeit import Timer
import argparse
from Lightswitch import Lightswitch

#shared variables
multiplex=Semaphore(5)
turnstile=Semaphore(1)
rope=Semaphore(1)
e_switch=Lightswitch()
w_switch=Lightswitch()

#for majority rules
west=0#number of baboons from west
east=0#number of baboons from east
status=0# 0:neutral 1:west rule 2:east rule 3:transition to west rule 4:transition to east rule
mutex=Semaphore(1)
westTurn=Semaphore(1)#turnstile for west
eastTurn=Semaphore(1)#turnstile for east
westLine=Semaphore(0)#line for waiting baboons from west
eastLine=Semaphore(0)#line for waiting baboons from east

threshold=0.5#for majority rule

#code for baboon crossing from west
def wCross(rng):
#     turnstile.acquire()
    w_switch.lock(rope)
#     turnstile.release()
    
    multiplex.acquire()
    #cross rope
    sleep(rng.random()+0.5)
    multiplex.release()
    
    w_switch.unlock(rope)
    
#code for baboon crossing from east
def eCross(rng):
#     turnstile.acquire()
    e_switch.lock(rope)
#     turnstile.release()
    
    multiplex.acquire()
    #cross rope
    sleep(rng.random()+0.5)
    multiplex.release()
    
    e_switch.unlock(rope)
    
def goFromWest(rng):
    global west
    global east
    global status
    global threshold
    westTurn.acquire()
    westTurn.release()
    
    mutex.acquire()
    west+=1
    
    if status==0:#check the declaration of status to get the meaning
        status=1
        mutex.release()
    elif status==2:
        if west>(int)((east+west)*threshold):#where rope is ceded
            status=3
            eastTurn.acquire()
        mutex.release()
        westLine.acquire()
    elif status==3:
        mutex.release()
        westLine.acquire()
    else:
        mutex.release()

    wCross(rng)
    
    mutex.acquire()
    west-=1
    if west==0:
        if status==4:
            eastTurn.release()
        if east:
            for _ in range(east):
                eastLine.release()
            status=2
        else:
            status=0
            eastTurn.release()
    if status==1:
        if east>(int)((east+west)*threshold):
            status=4
            westTurn.acquire()
    mutex.release()
    
    sleep(rng.random()+0.5)#wait to cross back 
    
def goFromEast(rng):
    global east
    global west
    global status
    global threshold
    eastTurn.acquire()
    eastTurn.release()
    
    mutex.acquire()
    east+=1
    if status==0:#check the declaration of status to get the meaning
        status=2
        mutex.release()
    elif status==1:
        if east>(int)((east+west)*threshold):
            status=4
            westTurn.acquire()
        mutex.release()
        eastLine.acquire()
    elif status==4:
        mutex.release()
        eastLine.acquire()
    else:
        mutex.release()
        
    eCross(rng)
    
    mutex.acquire()
    east-=1
    if east==0:
        if status==3:
            westTurn.release()
        if west:
            for _ in range(west):
                westLine.release()
            status=1
        else:
            status=0
            westTurn.release()
    if status==2:
        if west>(int)((east+west)*threshold):
            status=3
            eastTurn.acquire()
        
    mutex.release()
    
    sleep(rng.random()+0.5)#wait to cross back
    
def baboon(num,cross):
    rng=random.Random()
    rng.seed(num)
    if rng.random()<0.5:#start crossing from west
        for _ in range((int)(cross/2)):  
#             print("baboon {} goes from west".format(num))  
            goFromWest(rng)
#             print("baboon {} goes from east".format(num))
            goFromEast(rng)
    else:#start crossing from east
        for _ in range((int)(cross/2)):
#             print("baboon {} goes from east".format(num))
            goFromEast(rng)
#             print("baboon {} goes from west".format(num))  
            goFromWest(rng)
    
def totime(num,cross):
    bab="baboon("+str(num)+","+str(cross)+")"
    timer=Timer(bab,"from baboon2 import baboon")
    tim=timer.timeit(1)
    print("Baboon {} finished in {:0.4f}s".format(num,tim))

def total(baboon,cross,thres):
    print("Timing 3 simulations with {} baboons, {} crossings:".format(baboon,cross))
    global threshold
    threshold=thres
    for _ in range(3):
        print('-'*30)
        ts=[Thread(target=totime,args=[j,cross]) for j in range(baboon)]
        for t in ts:t.start()
        for t in ts:t.join()
        
    
if __name__=='__main__':
    parser = argparse.ArgumentParser(description='Baboon Crossing Rope2')
    parser.add_argument('--baboon','-b',
                        type=int,
                        default=10,
                        help='number of baboons',
                        metavar='numBaboon')
    parser.add_argument('--cross','-c',
                        type=int,
                        default=6,
                        help='number of crossings per baboon',
                        metavar='numCross')
    parser.add_argument('--threshold','-t',
                        type=float,
                        default=1.0,
                        help='threshold for majority rule',
                        metavar='threshold')
    args = parser.parse_args()
    tot="total("+str(args.baboon)+","+str(args.cross)+","+str(args.threshold)+")"
    timer=Timer(tot,"from baboon2 import total")
    tim=timer.timeit(1)
    print("-"*30)
    print("Total time elapsed: {}".format(tim))
    print("Avg. time per run: {}".format(tim/3))
    
    
        
        
        
