import random
from threading import Thread,Semaphore
from time import sleep
import argparse

rng=random.Random()
rng.seed(100)

stash=0
balls_on_field=0
N=0

stashMutex=Semaphore(1)
ballsMutex=Semaphore(1)
replenish=Semaphore(0)
finishReplenish=Semaphore(0)
cartOut=Semaphore(1)

#golfer
def golfer(num):
    global stash
    global N
    global balls_on_field
    while True:
        print ('Golfer ',num, ' calling for bucket')  
                    
        stashMutex.acquire()   
        
        if stash-N<0:
            replenish.release()#when not enough balls in stash, require cart out to replenish
            finishReplenish.acquire()#wait for stash being replenished
            
        stash-=N
        stashMutex.release()

        print ('Golfer ',num, ' got ',N,' balls; Stash = ',stash)
        for i in range(0,N):    
            #simulate 'swinging' here with random sleep
            sleep(rng.random()+0.5)
            ballsMutex=Semaphore(1)
            balls_on_field+=1
            ballsMutex=Semaphore(1)
            print ('Golfer ',num,' hit ball ',i)
            cartOut.acquire()#when cart is out, golfer is interrupted.
            cartOut.release()#if cart is not out, continue playing
        
        
#cart
def cart():
    global stash
    global balls_on_field
    while True:
        replenish.acquire()#wait for the signal to go out for replenish
        cartOut.acquire()#preempt playing golfers
        print ('#'*60)
        print ("Stash = ",stash,"; cart entering field")  
        stash+=balls_on_field
        print ('Cart done, gathered ',balls_on_field,' balls; Stash=', stash)
        balls_on_field=0
        print ('#'*60)
        cartOut.release()#signal golfers that they can continue
        finishReplenish.release()#signal golfers that they can come to get new bucket

        
if __name__=="__main__":
    
    parser = argparse.ArgumentParser(description='IIT Driving Range Simulator')
    parser.add_argument('--stash','-s',
                        type=int,
                        default=20,
                        help='initial number of balls in stash',
                        metavar='stash')
    parser.add_argument('--bucket','-b',
                        type=int,
                        default=5,
                        help='number of balls in one bucket',
                        metavar='N')
    parser.add_argument('--player','-p',
                        type=int,
                        default=3,
                        help='number of golfers playing',
                        metavar='numPlayers')
                        
    args = parser.parse_args()
    
    stash=args.stash
    N=args.bucket
    numPlayers=args.player

    ts=[Thread(target=golfer,args=[i]) for i in range(numPlayers)]
    c=Thread(target=cart)
    c.start()
    for t in ts:t.start()    
    

