import random
from threading import Thread,Semaphore
from time import sleep
import argparse
from itertools import cycle
from collections import deque


rng=random.Random()
rng.seed(100)

leaderLine=deque()
followerLine=deque()
leaderDance=deque()
followerDance=deque()
nextLeader=-1
nextFollower=-1

leaderTurnstile=Semaphore(1)#leader comes one by one
followerTurnstile=Semaphore(1)#follower comes one by one
leaderCome=Semaphore(0)#rendezvous
followerCome=Semaphore(0)#rendezvous
leaderCome1=Semaphore(0)#rendezvous
followerCome1=Semaphore(0)#rendezvous
leaderCome2=Semaphore(0)#rendezvous
followerCome2=Semaphore(0)#rendezvous
danceMutex=Semaphore(1)#mutex
noDancer=Semaphore(0)#for band to change music
changeMusic=Semaphore(1)#for band to change music
mutex=Semaphore(1)#mutex
mutex1=Semaphore(1)#mutex
mutex2=Semaphore(1)#mutex
mutex3=Semaphore(2)#mutex

def bandLeader(time):
    for music in cycle(['waltz','tango','foxtrot']):
        start_music(music)   
        sleep(time)
        changeMusic.acquire()#once band wants to change music, nobody is allowed to enter floor
        noDancer.acquire()#once nobody is in the floor, band can end music and change it
        end_music(music)
        changeMusic.release()
        
def start_music(music):
    print ('** Band leader started playing ', music,' **')

def end_music(music):
    print ('** Band leader stopped playing', music,'**')

def leader(num):
    leaderLine.append(num)
    while True:
        enter_floor('Leader')
        dance('Leader')
        line_up('Leader',num)
        
def follower(num):
    followerLine.append(num)
    while True:
        enter_floor('Follower')
        dance('Follower')
        line_up('Follower',num)
        
def enter_floor(role):
    global nextLeader
    global nextFollower
    
    if role=='Leader':#rendezvous, once leader and follower are ready, we can go
        leaderCome2.release()
        followerCome2.acquire()
    else:
        followerCome2.release()
        leaderCome2.acquire()
        
    mutex.acquire()#detect if band wants to change music
    changeMusic.acquire()#once band want to change music, nobody is allowed to enter floor
    changeMusic.release()#if band does not have that wish, everybody is allowed to enter floor
    mutex.release()
    if role=='Leader':
        if leaderLine:
            leaderTurnstile.acquire()#leader can only enter one by one
            nextLeader=leaderLine.popleft()
            print (role,nextLeader, 'entering floor.')   
    else:
        if followerLine:
            followerTurnstile.acquire()#follower can only enter one by one
            nextFollower=followerLine.popleft()
            print (role,nextFollower, 'entering floor.')

def dance(role):
    global nextLeader
    global nextFollower
  
    if role=='Leader':#rendezvous, once leader and follower are ready, we can go
        leaderCome.release()
        followerCome.acquire()
    else:
        followerCome.release()
        leaderCome.acquire()
        
    if role=='Leader':#only leader can lead dance
        print('Leader',nextLeader,'and Follower',nextFollower,'are dancing.')
        mutex1.acquire()
        leaderDance.append(nextLeader)
        followerDance.append(nextFollower)
        mutex1.release()
        nextLeader=-1
        nextFollower=-1
        leaderTurnstile.release()
        followerTurnstile.release()
        sleep(rng.random()+0.5)
        
    if role=='Leader':#rendezvous, once leader and follower finish, we can go
        leaderCome1.release()
        followerCome1.acquire()
    else:
        followerCome1.release()
        leaderCome1.acquire()
    
def line_up(role,num):
    global nextLeader
    global nextFollower
    sleep(rng.random()+0.5)#get back at leisure
    
    if role=='Leader':
        mutex2.acquire()
        if leaderDance.count(num)!=0:#if thread does not overlap, do this to allow max concurrency
            leaderDance.remove(num)
        else:
            num=leaderDance.pop()#if thread overlaps, do this to ensure nobody disappears
        leaderLine.append(num)
        print("Leader",num,'getting back in line')
        mutex2.release()
       
    else:
        mutex3.acquire()
        if followerDance.count(num)!=0:
            followerDance.remove(num)
        else:
            num=followerDance.pop()
        followerLine.append(num)
        print("Follower",num,'getting back in line')
        mutex3.release()
    
    mutex1.acquire()
    if not leaderDance and not followerDance:
        noDancer.release()
    mutex1.release()
    
if __name__=="__main__":
 
    
    parser = argparse.ArgumentParser(description='Dance mixer')
    parser.add_argument('--leader','-l',
                        type=int,
                        default=2,
                        help='number of leaders',
                        metavar='numLeader')
    parser.add_argument('--follower','-f',
                        type=int,
                        default=5,
                        help='number of followers',
                        metavar='numFollower')
    parser.add_argument('--time','-t',
                        type=int,
                        default=5,
                        help='lasting time(seconds) between music changes',
                        metavar='time')
                        
    args = parser.parse_args()

    leaders=[Thread(target=leader,args=[i]) for i in range(args.leader)]
    followers=[Thread(target=follower,args=[i]) for i in range(args.follower)]
    band=Thread(target=bandLeader,args=[args.time])        
    band.start()
    for t in leaders:t.start() 
    for t in followers:t.start()
      
   
    