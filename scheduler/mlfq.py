# MLFQ scheduler implementation

from __future__ import print_function
from collections import deque
import rr

class MLFQScheduler:
    def __init__(self, quanta,optimal):
        self.quanta = quanta
        self.optimal = optimal
        self.level=len(quanta)   #number of queues
        self.queues = []
        for i in range(self.level):
            self.queues.append(rr.RoundRobinScheduler(self.quanta[i]))
        self.jobInQ=dict()  #key is jid, value is queue id
        self.jobDown=dict() #key is jid, value is how many times a job is switched down
        self.jobSwitch=dict()   #key is jid, value is how many times a job is switched up or down
        self.isJobAtBottom=dict()    #key is jid, vlaue is true if job is at lowest priority queue.
        self.currentJob=None    #currently running job id

    # called when a job is first created -- the job is assumed
    # to be ready at this point (note: job_ready will not be called
    # for a job's first CPU burst)
    def job_created(self, jid):
        self.queues[0].job_created(jid)
        self.jobInQ[jid]=0  #initialize the job at queue 0
        self.jobDown[jid]=0 #initialize parameter
        self.jobSwitch[jid]=0   
        self.isJobAtBottom[jid]=False
        for q in range(0,self.level):
            self.queues[q].preempt_cnts[jid]=0
                    

    # called when a job becomes ready after an I/O burst completes
    def job_ready(self, jid):
        qid=self.jobInQ.get(jid)
        self.queues[qid].job_ready(jid)

    # called when a job's current CPU burst runs to the end of its
    # allotted time quantum without completing -- the job is
    # still in the ready state
    def job_quantum_expired(self, jid):
        qid=self.jobInQ.get(jid)
        if qid<self.level-1:    
            qid+=1   #job switches down   
            self.jobInQ[jid]=qid
            self.jobDown[jid]+=1
        else: #at the bottom, cannot switch down
            self.isJobAtBottom[jid]=True
                        
        self.queues[qid].job_quantum_expired(jid)
        
    # called when a job is preempted mid-quantum due to our
    # returning True to `needs_resched` -- the job is still in
    # the ready state
    def job_preempted(self, jid):
        qid=self.jobInQ[jid]
        self.queues[qid].job_preempted(jid)
        

    # called after a job completes its final CPU burst -- the job
    # will never become ready again
    def job_terminated(self, jid):
        self.jobSwitch[jid]+=self.jobDown[jid]
        
    # called when a job completes its CPU burst within the current
    # time quantum and has moved into its I/O burst -- the job is
    # currently blocked
    def job_blocked(self, jid,cpu_burst):
        if self.jobDown[jid] or self.isJobAtBottom[jid]:    #don't need to switch up
            self.jobSwitch[jid]+=self.jobDown[jid]
            self.jobDown[jid]=0
            self.isJobAtBottom[jid]=False
        else:                
            if self.jobInQ[jid]>0:
                #optimization if cpu burst greater than threshold percent of quantum but less than quantum
                qid=self.jobInQ[jid]
                quantum=self.quanta[qid-1]
                if cpu_burst<self.optimal*quantum:
                    self.jobInQ[jid]-=1
                    self.jobSwitch[jid]+=1
                    #print (self.optimal,cpu_burst,self.optimal*quantum)
        
    # called by the simulator after new jobs have been made ready.
    # we should return True here if we have a more deserving job and
    # want the current job to be preempted; otherwise return False
    def needs_resched(self):
        newQ=self.level #if no job is ready, newQ is set to max to prevent from unnecessary reschedule
        newJob=None
        
        for qid in range(self.level):
            if self.queues[qid].ready_queue:
                newQ=qid
                newJob=self.queues[qid].ready_queue[0]
                break
        if newQ<self.jobInQ[self.currentJob]:   #new queue has higher priority
            return True
        else:
            return False


    # return a two-tuple containing the job ID and time quantum for
    # the next job to be scheduled; if there is no ready job,
    # return (None, 0)
    def next_job_and_quantum(self):
        for qid in range(self.level):
            if self.queues[qid].ready_queue:
                (jid,quantum)=self.queues[qid].next_job_and_quantum()
                self.currentJob=jid
                return (jid,quantum)
        return (None, 0)

    # called by the simulator after all jobs have terminated -- we
    # should at this point compute and print out well-formatted
    # scheduler statistics
    def print_report(self):
        print('  JID | # Preempts | # Switches')
        print('-'*33)
        self.jobPreempt= dict()
        for jid in sorted(self.jobSwitch.keys()):
            self.jobPreempt[jid] = 0
            for qid in range(self.level):
                self.jobPreempt[jid] += self.queues[qid].preempt_cnts[jid]
            print('{:5d} | {:10d} | {:6d}'.format(jid, self.jobPreempt[jid], self.jobSwitch[jid]))
        print('-'*33)
        avgPreempt = sum(self.jobPreempt.values(), 0.0) / len(self.jobPreempt)
        avgSwitch = sum(self.jobSwitch.values(), 0.0) / len(self.jobSwitch)
        print('  Avg | {:10.2f} | {:6.2f}'.format(avgPreempt, avgSwitch))
        print()
        
        print('  QID | Avg length | Max length')
        print('-'*33)
        for qid in range(self.level):
            if self.queues[qid].queue_lengths:
                avgQlen = sum(self.queues[qid].queue_lengths, 0.0) / len(self.queues[qid].queue_lengths)
                maxQlen = max(self.queues[qid].queue_lengths)
            else:
                avgQlen,maxQlen=0,0
            print('{:5d} | {:10.2f} | {:10.2f}'.format(qid, avgQlen,maxQlen))
        print()
